<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Información de autor</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>
<?php
  $id = $_GET['id'];
  $error = false;
  if (empty($id)) {
    $error = true;
?>
  <p>Error, no se ha indicado id del autor</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select id_autor, nombre_autor
      from biblioteca.autor
      where id_autor = '".$id."';";

    $autor = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($autor) == 0) {
      $error = true;
?>
  <p>No se ha encontrado algún autor con el Id: <?php echo $isbn; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($autor, null, PGSQL_ASSOC);
      $nombre = $tupla['nombre_autor'];
?>
<table>
  <caption>Información del autor</caption>
  <tbody>
    <tr>
      <th>Id</th>
      <td><?php echo $id; ?></td>
    </tr>
    <tr>
      <th>Nombre</th>
      <td><?php echo $nombre; ?></td>
    </tr>
    <tr>
      <th>Libro/s</th>
      <td>
<?php
      $query = "select titulo_libro
        from biblioteca.libro_autor la
        inner join biblioteca.libro as l
          on (la.isbn = l.isbn and la.id_autor = '".$id."');";

      $autores = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($autores) == 0) {
?>
        <p>Sin Libros</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($autores, null, PGSQL_ASSOC)) {
          foreach ($tupla as $atributo) {
?>
          <li><?php echo $atributo; ?></li> 
<?php
          }
        }
?>
        </ul>
<?php
      }
?>
    </tr>
    
<?php
      }
    }
  
?>
  </tbody>
</table>

<?php
  pg_free_result($result);
  pg_close($dbconn);

  if (!$error) {
?>
<form action="delete-autor.php" method="post">
  <input type="hidden" name="id" value="<?php echo $id; ?>" />
  <p>¿Está seguro/a de eliminar este autor?</p>
  <input type="submit" name="submit" value="DELETE" />
  <p>
    
    Se borrarán a su vez todos las relaciones con libro_autor
  </p>
</form>

<form action="autores.php" method="post">
  <input type="submit" name="submit" value="Cancelar" />
</form>
<?php
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="autores.php">Lista de autores</a></li>
</ul>

</body>
</html>
