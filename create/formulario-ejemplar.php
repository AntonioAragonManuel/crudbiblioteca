<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Formulario de ejemplar</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>

<form action="create-ejemplar.php" method="post">
<table>
  <caption>Información de ejemplar</caption>
  <tbody>
    <tr>
      <th>Clave</th>
      <td><input type="text" name="clave" /></td>
    </tr>
    <tr>
        <th>Isbn </th>
        <td> <input type="text" name ="isbn"/></td>
    </tr>
    <tr>
      <th>Conservacion</th>
      <td><textarea name="conservacion"></textarea></td>
    </tr>
  </tbody>
</table>
<input type="submit" name="submit" value="CREATE" />
</form>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
</ul>

</body>
</html>