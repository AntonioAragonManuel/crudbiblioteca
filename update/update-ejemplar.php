<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Update Ejemplar</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>

<?php
  $error = false;
  $isbn = $_POST['isbn'];
  $clave = $_POST['clave'];
  $conservacion = $_POST['conservacion'];

  if (empty($isbn)) {
    $error = true;
?>
  <p>Error, no se indico el ISBN del Ejemplar</p>
<?php
  }
  if (empty($clave)) {
    $error = true;
?>
  <p>Error, no se indicò la clave del ejemplar</p>
<?php
  }

  if (!$error) {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select isbn, clave_ejemplar, conservacion_ejemplar
      from biblioteca.ejemplar
      where isbn = '".$isbn."' and clave_ejemplar='".$clave."';";

    $ejemplar = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($ejemplar) == 0) {
?>
  <p>No se ha encontrado algún ejemplar con ISBN <?php echo $isbn; ?> y Clave <?php echo $clave ?></p>
<?php
    } else {
      $query = "update biblioteca.ejemplar
        set conservacion_ejemplar = '".$conservacion."'
        where isbn = '".$isbn."' and clave_ejemplar='".$clave."';";

      $resultado = pg_query($query) or die('La consulta falló: ' . pg_last_error());

      if (pg_affected_rows($resultado) == 0) {
?>
  <p>Error al momento de guardar los datos del ejemplar</p>
<?php
      } else {
?>
  <p>Los datos del ejemplar con ISBN <?php echo $isbn; ?> han sido actualizados con exito</p>
<?php
      }
    }
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="ejemplares.php">Lista de Ejemplares</a></li>
</ul>

</body>
</html>
